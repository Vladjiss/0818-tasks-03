#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#define MAX_ROWS 5
#define MAX_LENGTH 30
void Swap_rows(char** arr, int i, int j) {
	char* tmp = arr[i];
	arr[i] = arr[j];
	arr[j] = tmp;
}
void quickSort(char** arr, int* lens, int left, int right) {
	int i = left, j = right;
	int tmp;
	int pivot = lens[(left + right) / 2];
	while (i <= j) {
		while (lens[i] < pivot)
			i++;
		while (lens[j] > pivot)
			j--;
		if (i <= j) {
			tmp = lens[i];
			lens[i] = lens[j];
			lens[j] = tmp;
			Swap_rows(arr, i, j);
			i++;
			j--;
		}
	};
	if (left < j)
		quickSort(arr, lens, left, j);
	if (i < right)
		quickSort(arr, lens, i, right);
}
void SortIncrease(char** arr, int num) {
	int* lens = (int*)calloc(num, sizeof(int));
	for (int i = 0; i < num; i++) {
		int row_len = 0;
		while (arr[i][row_len] != '\0' && arr[i][row_len] != '\n') {
			row_len++;
		}
		lens[i] = row_len;
	}
	quickSort(arr, lens, 0, num - 1);
}
void Matrix_print(char** arr, int num) {
	for (int i = 0; i < num; i++) {
		int j = 0;
		while (arr[i][j] != '\0' && arr[i][j] != '\n') {
			printf("%c", arr[i][j]);
			j++;
		}
		printf("\n");
	}
}
int main() {
	setlocale(LC_ALL, "Russian");
	char **arr;
	int num = 0;
	arr = (char**)calloc(num, sizeof(char*));
	printf("Enter rows\n");
	while (num<MAX_ROWS) {
		char str[MAX_LENGTH];
		fgets(str, MAX_LENGTH, stdin);
		if (str[0] == '\n' || str[0] == '\0') {
			break;
		}
		num++;
		arr = (char**)realloc(arr, num * sizeof(char*));
		arr[num - 1] = (char*)calloc(MAX_LENGTH, sizeof(char));
		for (int i = 0; i < MAX_LENGTH; i++) {
			arr[num - 1][i] = str[i];
			if (str[i] == '\n' || str[i] == '\0')
				break;
		}
	}
	SortIncrease(arr, num);
	printf("Sort ascending:\n");
	Matrix_print(arr, num);
	for (int i = 0; i < num; i++)
		free(arr[i]);
	free(arr);
	return 0;
}
