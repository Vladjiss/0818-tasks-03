#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <time.h>
#define MAX_ROWS 3
#define MAX_LENGTH 30
void PrintRandomRow(char** arr, int num) {
	int n = rand() % num;
	int i = 0;
	while (arr[n][i] != '\0' && arr[n][i] != '\n') {
		printf("%c", arr[n][i]);
		i++;
	}
	printf("\n");
}
int main() {
	setlocale(LC_ALL, "Russian");
	srand(time(0));
	char **arr;
	int num = 0;
	arr = (char**)calloc(num, sizeof(char*));
	printf("Enter rows\n");
	while (num<MAX_ROWS) {
		char str[MAX_LENGTH];
		fgets(str, MAX_LENGTH, stdin);
		if (str[0] == '\n' || str[0] == '\0') {
			break;
		}
		num++;
		arr = (char**)realloc(arr, num * sizeof(char*));
		arr[num - 1] = (char*)calloc(MAX_LENGTH, sizeof(char));
		for (int i = 0; i < MAX_LENGTH; i++) {
			arr[num - 1][i] = str[i];
			if (str[i] == '\n' || str[i] == '\0')
				break;
		}
	}
	printf("Randon row:\n");
	PrintRandomRow(arr, num);
	for (int i = 0; i < num; i++)
		free(arr[i]);
	free(arr);
	return 0;
}
