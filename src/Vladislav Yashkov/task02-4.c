#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <time.h>
#define MAX_ROWS 5
#define MAX_LENGTH 30
void Swap_rows(char** arr, int i, int j) {
	char* tmp = arr[i];
	arr[i] = arr[j];
	arr[j] = tmp;
}
void SortRandom(char** arr, int num) {
	for (int i = 0; i < num; i++) {
		int random = rand() % (num - i) + i;
		Swap_rows(arr, i, random);
	}
}
void Matrix_print(char** arr, int num) {
	for (int i = 0; i < num; i++) {
		int j = 0;
		while (arr[i][j] != '\0' && arr[i][j] != '\n') {
			printf("%c", arr[i][j]);
			j++;
		}
		printf("\n");
	}
}
int main() {
	setlocale(LC_ALL, "Russian");
	srand(time(0));
	char **arr;
	int num = 0;
	arr = (char**)calloc(num, sizeof(char*));
	printf("Enter rows\n");
	while (num<MAX_ROWS) {
		char str[MAX_LENGTH];
		fgets(str, MAX_LENGTH, stdin);
		if (str[0] == '\n' || str[0] == '\0') {
			break;
		}
		num++;
		arr = (char**)realloc(arr, num * sizeof(char*));
		arr[num - 1] = (char*)calloc(MAX_LENGTH, sizeof(char));
		for (int i = 0; i < MAX_LENGTH; i++) {
			arr[num - 1][i] = str[i];
			if (str[i] == '\n' || str[i] == '\0')
				break;
		}
	}
	SortRandom(arr, num);
	printf("Sort Random:\n");
	Matrix_print(arr, num);
	for (int i = 0; i < num; i++)
		free(arr[i]);
	free(arr);
	return 0;
}
